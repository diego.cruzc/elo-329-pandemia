import javafx.scene.control.*;

public class SimulatorMenuBar extends MenuBar {
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        Menu configure = new Menu("Settings");
        getMenus().addAll(controlMenu, configure);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        MenuItem settings = new MenuItem("Settings");
        controlMenu.getItems().addAll(start,stop);
        configure.getItems().add(settings);
        start.setOnAction(e-> simulator.start());
        stop.setOnAction(e-> simulator.stop());
        settings.setOnAction(e -> simulator.settings());
    }
}
